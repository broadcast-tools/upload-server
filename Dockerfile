FROM node:lts
LABEL maintainer="Stéphane Ludwig <gitlab@stephane-ludwig.net>"

ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH:-dev}

WORKDIR /app

COPY package*.json /app/
RUN npm install

COPY server.js /app/

EXPOSE 3000

CMD ["npm", "start"]
