import os from 'node:os';
import process from 'node:process';
import fs from 'node:fs';

import express from 'express';
import cors from 'cors';
import formidable from 'formidable';
import uuid4 from 'uuid4';

const GIT_HASH= process.env.GIT_HASH;
console.log(`Git Hash: ${GIT_HASH}`);

const port = process.env.PORT || 3000;
const uploadPath = process.env.UPLOAD_PATH || os.tmpdir();
const maxFileSize = 200 * 1024 * 1024 * 1024;  // FileSize 200GB

if (!fs.existsSync(uploadPath)) {
  console.log(`Upload path ${uploadPath} does not exist`);
  process.exit(1);
}

const app = express();
app.use(cors());

app.get('/', async (req, res) => {
  res.send(`<h2>Upload Server</h2>
        <form action="/" enctype="multipart/form-data" method="post">
            File: <input type="file" name="file" />
            <input type="submit" value="Upload" />
        </form>`)
});

app.post('/', async (req, res, next) => {
  const form = formidable({
    keepExtensions: true,
    uploadDir: uploadPath,
    maxFileSize: maxFileSize,
    filename: function (name, ext) {
      const id = uuid4()
      return `${id}${ext.toLowerCase()}`
    }
  });

  await form.parse(req, (err, fields, files) => {
    console.log(fields);
    console.log(files);
    const file = files.file[0];

    if (err) {
      console.log('Error during file upload:', err);
      next(err);
    }

    res.json(file);
  })
});

const server = app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
})
server.requestTimeout = 0;
